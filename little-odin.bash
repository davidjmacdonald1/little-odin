#!/bin/bash

if [[ "$0" != "./little-odin.bash"  ]]; then
	echo "Please run ./little-odin.bash from the local directory"
	exit 1
fi

if [[ ! -d "tmp" ]]; then
	mkdir "tmp"
fi

if [[ ! -f ./odin-path || "$(cat ./odin-path)" == "" ]]; then
	echo "Please run 'echo PATH_TO_ODIN_CMD > odin-path' to begin"
	exit 1
fi
ODIN="$(cat ./odin-path)"
ODIN="$(realpath "${ODIN/#\~/$HOME}")"

exercises="./exercises"
if [[ "$#" -eq 0 ]]; then
	indices=({000..999})
else
	indices=()
	for arg in "$@"; do
		if [[ $arg == "test" ]]; then
			exercises="./answers"
			if [[ "$#" -lt 2 ]]; then
				indices=({000..999})
			fi
		elif ! [[ $arg =~ ^[0-9]+$ ]]; then
			echo "Invalid argument: $arg. Please provide only exercise numbers"
			exit 1
		else
			mapfile -t -O "${#indices[@]}" indices < <(printf "%03d" "$((10#$arg))")
		fi
	done
fi

RED="\033[0;31m"
GREEN="\033[0;32m"
YELLOW="\033[0;33m"
NC="\033[0m"

cd "$exercises" || exit 1
echo -e "Welcome to Little Odin! \n"
for i in "${indices[@]}"; do
	for file in "$i"_*.odin; do
		if [[ -e "$file" ]]; then
			valid="../tmp/${i}isValid"
			if [[ -e "$valid" ]]; then
				echo "Checking: $file"
				echo -e "${GREEN}ALREADY PASSED\n${NC}"
				sleep .01
				continue
			fi

			main="../tmp/exercise$i"
			echo "Compiling: $file"
			if ! eval "$ODIN" build "$file" -file -out:"$main"; then
				echo -e "${YELLOW}Take a look at exercises/$file"
				exit 1
			fi

			echo "Checking: $file"
			stdout=$(eval "$main")
			expected=$(eval "../expected.bash" "$(printf "%03d" "$((10#$i))")")
			if [[ "$stdout" == "$expected" ]]; then
				echo -e "${GREEN}PASSED: $stdout\n${NC}"
				touch "$valid"
			else
				echo -e "${RED}ERROR:"
				echo "========= EXPECTED ======================="
				echo -e "${NC}$expected${RED}"
				echo "========= BUT FOUND ======================"
				echo -e "${NC}$stdout${RED}"
				echo "=========================================="

				echo -e "\nFix exercises/$file and try again"
				exit 1
			fi
		fi
	done
done
