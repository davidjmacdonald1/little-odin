# Little Odin
This project is inspired by [Ziglings](https://codeberg.org/ziglings/exercises),
a series of small programs for you to fix in order to learn Zig.
Here, we'll be learning [Odin](https://odin-lang.org/).

(Big thanks to ThePrimeagen for introducing me to Ziglings!)

## Installation
First, [install Odin](https://odin-lang.org/docs/install/).
Then, clone this repository wherever you wish:
```bash
$ cd SOMEWHERE_NICE
$ git clone https://gitlab.com/davidjmacdonald1/little-odin.git
$ cd little-odin
```
Now, you will need to store the path to your Odin executable in a file
(because bash aliases are hard); call the file `odin-path`.
You can find your path by running `which odin`.

You are now ready to start!

## Usage
You can run `./little-odin.bash` to begin.
This script will run through all the exercises one at a time from 0 upwards.
If there is a problem, it will stop and it's your job to fix it.

To run specific exercises, you can include them as arguments.
For example, to run exercises 002 and 034, you can run:
```bash
$ ./little-odin.bash 2 034

```

When you complete an exercise correctly, Little Odin remembers it and will save
time by not checking that one again.
To make Little Odin forget an exercise and check it again, you can delete
`tmp/###isValid` where `###` is the exercise number.
Alternatively, you can delete the entire `tmp` directory to forget all of them.
