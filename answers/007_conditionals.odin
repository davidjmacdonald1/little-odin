/*
	Often, we want the entire control flow of the program to change based
	on a condition, not just an assignment.

	In Odin, some block statements can use the "do" keyword to execute one
	statement, rather than curly brackes.
*/
package main

import "core:fmt"

main :: proc() {
	x := 4
	if x > 3 do x = 3

	// We can also use the when keyword to perform compile-time conditionals
	os_version :: 2
	when os_version < 3 {
		// stuff
	}

	// Make this print "booleans huh?"
	z := [?]bool{true, false, false, true}
	if z[0] {
		fmt.println("booleans huh?")
	} else {
		fmt.println("nothing")
	}
}
