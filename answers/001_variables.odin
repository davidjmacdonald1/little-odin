/*
	This program was supposed to print 12, so let's see what went wrong.

	In Odin, variables can be declared like: "name: type = value".
	These are values in memory that can be reassigned: "name = newValue".

	- If the type can be inferred, you can just say "name := value"
	- If you do not include a value, the variable will be zero initialized
	and you must include a type: "name: type"
	- If you don't want to zero initialize, you must explicitly opt-into it by
	using "---" to mean undefined: "name: type = ---"
*/
package main

import "core:fmt"

main :: proc() {
	x := -1
	x = 12
	fmt.println(x)
}
