/*
	We've seen the main procedure and we've called fmt.print procedures,
	but let's see how we can make our own!
*/
package main

import "core:fmt"

main :: proc() {
	fmt.println(add(1, 2))
}

// This function is supposed to add two parameters: x and y.
// Notice that Odin allows file-level procedures to be declared after it's used.
// When two parameters share the same type, only the last one needs a type decl.
// Parameters are immutable (cannot be modified); you must copy them to modify.
add :: proc(x, y: int) -> int {
	return x + y
}
