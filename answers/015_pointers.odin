/*
	Pointers are a type that stores a memory address.
	These are fundamental to memory management and mutability across procedures.

	The zero value for pointers (and similar types) is nil.
*/
package main

import "core:fmt"

main :: proc() {
	x: int = 5
	ptr: ^int = &x // & gets the memory address of a var. ^t is a pointer to t

	// The value stored at the address can be accessed with the dereference op:
	// ptr^

	x += 1 // Changes to x will be reflected in ptr^
	ptr^ -= 1 // Changes to ptr^ will be reflected in x

	// In Odin, proc arguments are passed by value.
	// Because of that, this proc call doesn't double x.
	// Change double so that it works using pointers.
	double(ptr)
	fmt.println(x)
}

// Make this fuction successfully double an integer
// Don't forget to update the proc call!
double :: proc(n: ^int) {
	n^ *= 2
}
