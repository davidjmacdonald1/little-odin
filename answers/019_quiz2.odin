/*
	We've seen a lot in this section: casting, procedures, slices,
	dynamic arrays, and memory management.

	Let's test our knowledge!
*/
package main

import "core:fmt"

main :: proc() {
	arr := make([dynamic]f16, 8) // allocate the array with size and capacity 8
	assert(len(arr) == cap(arr) && len(arr) == 8)
	defer delete(arr) // deallocate the array

	fibonacci_fill(arr[:]) // fill the entire array with fibonacci numbers
	append(&arr, 0, 0) // add two zeroes to the end of the array

	fmt.println(arr)
}

// There are some mistakes in this fibonacci procedure.
// It is supposed to fill a slice of f16 with fibonacci numbers: 1, 1, 2, 3, ...
fibonacci_fill :: proc(out: []f16) {
	x0, x1 := 1, 1
	for _, i in out {
		out[i] = f16(x0)
		x2 := x0 + x1
		x0, x1 = x1, x2
	}
}
