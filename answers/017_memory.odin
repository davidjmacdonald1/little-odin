/*
	All of the variables we have used so far have been stored on the stack.
	This region of of memory is automatically cleaned up after the scope ends.

	That is a double edged sword; you (probable) can't cause a memory leak
	using only the stack. But, you cannot extend the lifetime of a variable
	beyond the scope it was declared.

	If we want dynamic-lifetime varibles, we can allocate on the heap instead.
	Just remember, this memory needs to be deallocated eventually.
	Your OS will free it when the program ends, but if your program will run
	for a long time, you must free the memory you allocate to avoid memory
	leaks. Here, we can see the power of defer.
*/
package main

import "core:fmt"

main :: proc() {
	// A slice internally contains a pointer. Here, we can use make to allocate
	// its backing memory; this slice will have len 10
	slice := make([]int, 10) // make will alloc backing memory
	defer delete(slice) // delete will dealloc backing memory

	// Notice, using a defer, we can place the dealloc next to the alloc.
	// This makes it much clearer that the memory will be dealloced, and
	// defer also ensures it will be dealloced if the program panics.

	// To allocate most types, you want to use new.
	x := new(int) // alloctes an integer and returns a pointer
	defer free(x) // free will dealloc a pointer
	x^ = 10

	// We can condense the previous 3 lines using new_clone
	x2 := new_clone(42) // new_clone infers the type and writes the value
	defer free(x2)

	// Make this print 42
	fmt.println(x2^)
}
