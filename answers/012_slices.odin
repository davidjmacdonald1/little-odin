/*
	Slices are similar to arrays, but they are references to a memory section.
	They do not have a known compile-time size, but they store their size at
	runtime.
	The "len()" proc can give you the length of a slice (or array or string or)
	and it will be O(1) access.
*/
package main

import "core:fmt"

main :: proc() {
	arr := [?]int{0, 1, 2, 3, 4, 5, 6}

	// Slices use [] for their type.
	slice: []int = arr[:] // Slice operator to get slice from array. See below:

	// The slice operator [low:high] slices a memory region.
	// low is inclusive and high is exclusive.
	// If you exclude low, it will start the slice at 0
	// If you exclude high, it will end the slice at the end of the region.
	start := arr[:3]
	end := arr[3:]

	// The benefit of an exclusive upper bounds means that arr[:x] and arr[x:]
	// partition the entire arr.

	// Print end and start
	fmt.println(end, start)
}
