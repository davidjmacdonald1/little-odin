/*
	Procedures can return 0, 1, or many things.
	Let's see an example of multi-return values.
*/
package main

import "core:fmt"

main :: proc() {
	greetings := [?]string{"Hello", "hi", "hey", "Howdy"}

	// Odin let's you initialize within an if condition
	if i, ok := indexOf(greetings[:], "hi"); ok {
		fmt.println("Found hi at", i)
	} else {
		fmt.println("Did not find hi")
	}
}

// indexOf will return the index of s in slice and whether or not it exists.
// Notice that return values can be given a name.
indexOf :: proc(slice: []string, s: string) -> (i: int, ok: bool) {
	for str, j in slice {
		// Return the index and true if s is found
		if str == s do return j, true
	}

	// Odin created vars i and ok in this scope.
	// We can say "return" with no values, and both of these will be returned.
	// By default, i and ok are 0 valued; so ok will be false
	return
}
