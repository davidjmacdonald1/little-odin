/*
	Uh oh! This exercise should print -1.

	In Odin, there are numerous data types; for now, let's look at integers.
	"int" will be the natural signed integer for your system, and
	"uint" will be the natural unsigned integer for your system.

	Odin includes specific-sized integers as well, such as "i8", "u8", "i128",
	"uintptr", and even "i32le" for little endian or "i32be" for big endian.
	Check out https://odin-lang.org/docs/overview/#basic-types for more.

	Numeric literal values such as "100.2" or "0xF" have types or sizes.
	Odin will allow you to make an assignment if the value can be expressed
	using the type you chose;
	so "x: int = 1.0" is valid, but "x: u8 = 256" is not.
*/
package main

import "core:fmt"

main :: proc() {
	x: int = -1.0
	fmt.println(x)
}
