#!/bin/bash

case "$1" in
	"000")
		echo "Hellope!" ;;
	"001")
		echo "12" ;;
	"002")
		echo "-1" ;;
	"003")
		echo "3.14" ;;
	"004")
		echo "22" ;;
	"005")
		echo "[9, 14, 18]" ;;
	"006")
		echo "23 is odd" ;;
	"007")
		echo "booleans huh?" ;;
	"008")
		echo "01234" ;;
	"009")
		echo "n= 5 i= 7" ;;
	"010")
		echo "4.5" ;;
	"011")
		echo "3" ;;
	"012")
		echo "[3, 4, 5, 6] [0, 1, 2]" ;;
	"013")
		echo "h 0 i 1 " ;;
	"014")
		echo "Found hi at 1" ;;
	"015")
		echo "10" ;;
	"016")
		echo "1 2 3 " ;;
	"017")
		echo "42" ;;
	"018")
		echo "[-2, 0, 2, 4, 6, 8]" ;;
	"019")
		echo "[1, 1, 2, 3, 5, 8, 13, 21, 0, 0]" ;;
	"020")
		echo "north is north" ;;
	*)
		echo -e "\033[0;35mExercise $1 has no solution yet!"
esac
