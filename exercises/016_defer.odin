/*
	Sometimes, we want to perform an action when a scope ends.
	This may be to close a file or connection, or to deallocate memory.

	To do this, Odin has a "defer" keyword to defer a statement until the end
	of the current scope or block.

	defer can be used in conjunction with if to check a condition at scope end:
	"defer if true {}"

	Note that if you do "if true { defer ... }" then the defer will run when
	the if statement's scope ends.

	You can think of multiple defers as being in a stack; last in is first out
*/
package main

import "core:fmt"

main :: proc() {
	// Make this print 1 2 3
	defer fmt.print("2", "")
	defer fmt.print("3", "")
	fmt.print("1", "")
}
