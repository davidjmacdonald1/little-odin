/*
	Constants are like variables except they cannot be modified, and do not have
	a location in memory; they may not even have a location in the binary as
	they are just compile-time values.

	Constants are declared as follows: "name : type : value".
	- If the type can be inferred, you can simply say "name :: value";
	- You must include a value.

	Notice that procedure declarations use this same syntax "main :: proc()...".

	Make this exercise print 3.14
*/
package main

import "core:fmt"

main :: proc() {
	my_pi: f16 : 3.14
	my_pi = 3
	fmt.println(my_pi)
}
