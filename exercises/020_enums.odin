/*
	Enums are an ordered collection of names with an associated value.
	They are very useful to avoid dozens of boolean flags, as each condition
	can be represented with a new name in an enum.
*/
package main

import "core:fmt"

// Each direction is assigned an integer automatically
Direction :: enum {
	North,
	East,
	South,
	West,
}

// You may specify the underlying size and specify value
Enum :: enum u8 {
	A, // blanks will have an automatic value
	B = 10,
	C = 66,
	D,
}

main :: proc() {
	d := Direction.West
	// d can hold any of these 4 directions
	if int(d) == 1 {
		fmt.println("d is east")
	}

	d = --- // make this print "north is north"
	if d == .North { 	// .North is inferred to be Direction.North
		fmt.println("north is north")
	} else if d == .East {
		fmt.println("north is east")
	} else if d == .South {
		fmt.println("north is south")
	} else {
		fmt.println("north is west probably")
	}
}
