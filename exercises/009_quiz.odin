/*
	Okay! That was a lot. Let's take a step back with a nice relaxing quiz.

	The following program is supposed to find the largest element of an array.
	We want both its index and the element itself, but there are some problems.

	Have a look and see what you can fix.
*/
package main

import "core:fmt"

main :: proc() {
	nums := [?]int{4, -2, -8, 0, -2, -19, 1, 5, 3}

	// Print the largest element and its index in the following format:
	// "n= # i= #" where # are the respective numbers

	result := [2]int{-1, 0}
	for i = 0; i < len(nums); i += 2 {
		when result[0] < 0 || nums[i] > result[1] {
			result[0] = i
			result[1] = nums[i]
		}

		break
	}

	fmt.println("n=", result, "i=", result)
}
