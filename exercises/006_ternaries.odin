/*
	Sometimes, we want to assign variables conditionally.

	We could use conditional statements, as we will see in the next exercise,
	but Odin also includes conditional expressions called ternaries.
*/
package main

import "core:fmt"

main :: proc() {
	x := 3
	evenOrOdd := "even" if x & 1 == 0 else "odd"
	// You can also use the familiar C-style syntax:
	// isEven := (x & 1 == 0)? "even" : "odd"

	// We can also perform conditional expressions at compile time with "when":
	n :: -20
	y :: n when n > 0 else -n

	z := y + x // z is set to 23

	// Change --- to make this print "23 is odd"
	fmt.println(z if --- else z + 1, "is", evenOrOdd)
}
