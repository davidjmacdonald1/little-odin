/*
	Welcome to Little Odin! Please help fix this code,
	which is supposed to greet you with "Hellope!".

	Odin doesn't have "functions" like many other languages do.
	Instead, it refers to functions as procedures;
	They are declared using the "proc" keyword.

	The main procedure is the entry point to your program.

	Can you now fix the code below?
*/
package main

import "core:fmt"

main :: func() {
	fmt.println("Hellope!")
}
