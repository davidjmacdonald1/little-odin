/*
	What if we want to repeat code? Then we need a loop! But what kind?
	Well, in Odin, we have access to for loops... and that's it.
	But wait! These aren't your granddaddy's for loops.
*/
package main

import "core:fmt"

main :: proc() {
	// Loop 0: The infinite loop or the "while true" loop.
	// Infinite loops will run until they encounter a break statement.
	x := 0
	for {
		if x >= 10 do break
		x += 1
	}

	// Loop 1: The conditional loop or the "while" loop
	// Conditional loops will break automatically when the condition is false
	x = 0
	for x < 10 {
		x += 1
	}

	// Loop 2: The full loop or the "for" loop
	// Full loops allow you to run a statement before the loop ever runs (init)
	//		or run a statement after each iteration (post).
	// Both of these are optional
	for x := 0; x < 10; x += 3 do fmt.print(x)
	// Make the above loop print 01234
}
