/*
	Odin supports the familiar arithmetic operations: -, +, *, /, %, etc.
	See https://odin-lang.org/docs/overview/#arithmetic-operators for more.

	Binary operators that take the form "a op b" can also be combined with the
	assignment operator like: "a op= b" to mean "a = a op b".

	We can assign the result of arithmetic to both variables and constants;
	however, constants require compile-time calculations.
*/
package main

import "core:fmt"

main :: proc() {
	// Change only "a := " or "b :: " to make this print 22
	a := 4 * 5 + 12
	b :: a + -1 - 9

	fmt.println(b)
}
