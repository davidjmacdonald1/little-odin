/*
	Odin has another type of loop: a for loop.
	In this case, this is a foreach loop; this is the most useful of them all.
*/
package main

import "core:fmt"

main :: proc() {
	// Suppose we want to loop over 0 to 9:
	for i := 0; i < 10; i += 1 {}

	// Instead, we can use ranges:
	for i in 0 ..< 10 {} 	// exclusive range
	for i in 0 ..= 9 {} 	// inclusive range

	// These loops can iterate over a lot of built in types
	slice := []int{0, 2, 4, 5, -10}
	for n in slice {} 	// n is the each element of the array
	for n, i in slice {} 	// we can add i as an index
	for _, i in slice {} 	// we can use _ to exclude the element

	// Print out h 0 i 1
	for --- in "hi" {
		fmt.print(c, i, "")
	}
}
