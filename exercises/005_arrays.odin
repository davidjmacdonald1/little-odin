/*
	Arrays are a collection of one particular type with a compile-time size.
	The array type is of the form "[n]type", where n is the number of elements.
	You can write [?]type to allow the compiler to infer the size of the array.

	You can index an array with the postfix "[i]" operator with an index i.
	By default, Odin performs bounds checking to verify the index is valid;
	however, you can disable this by writing your access inside of a
	"#no_bounds_check { arr[0] // no check }" block.

	Arrays of numbers can be added and multiplied element-wise.
	Make this code print out [9, 14, 18]
*/
package main

import "core:fmt"

main :: proc() {
	arr0 :: [2]int{3, 13}
	arr1 := [3]int{2, arr0[0], 5}
	arr2 := [?]int{7, 11, arr0[1]}
	fmt.println(arr1 * arr2)
}
