/*
	Sometimes, we want to convert between one type to another.
	Often, Odin forces us to do it in order to use operators with
	two different types.

	To cast between most types, we can just use "type(x)".
	For more complex types, we can also use "cast(type)x".

	Lastly, some of you may be familiar with the fast inverse square root
	algorithm found in Quake III. In it, a pointer (we will discuss later),
	is casted to another kind of pointer to get the underlying bits.

	Here, Odin has a built in "transmute(type)x" which converts the underlying
	bits rather than the high-level value.

	Note than untyped integer literals don't usually need to be casted,
	as they are implictily coerced to most numeric types.
*/
package main

import "core:fmt"

main :: proc() {
	x := int(one()) * 4
	f := .5 + x // fix this by casting x to f16, f32, or f64
	fmt.printf("%.1f\n", f)
}

one :: proc() -> f16 {
	return 1.0
}
