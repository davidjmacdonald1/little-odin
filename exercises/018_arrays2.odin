/*
	Sometimes, we want an array that can change in size at runtime.
	For this, Odin has dynamic arrays. These are another type that have backing
	memory that may be alloced/dealloced with make/delete.
*/
package main

import "core:fmt"
import "core:math/rand"

main :: proc() {
	n := int(rand.int31_max(30 - 10) + 10) // random int between 10 and 29
	arr := make([dynamic]int, n)
	defer delete(arr)

	// Here, arr will have n elements all initialized to 0
	// We can grab the length: "len(arr)"
	// We can index the array: "arr[9] = 4"
	// We can slice the array: "arr[2:]"

	// Using make, we can also give a dynamic array a specific capacity without
	// giving it a length.
	arr2 := make([dynamic]int, 0, n)
	defer delete(arr2)

	// Here, len(arr2) is 0 and arr2[0] is out of bounds.
	// We can use cap(arr2) and see it has a capacity of n.
	// To add elements to the array, we can use append and inject_at.

	// append will add elements to the array and resize it if necessary.
	// If the capacity is larger than the size, append can insert without a new
	// allocation.
	append(&arr2, 2, 4, 6) // append can add 1 or more elements at once.

	// inject_at will insert elements at a specific index and resize it
	// if necessary.
	inject_at(&arr2, 0, 0) // inject_at can inject 1 or more elements at once.

	// Make the following line print [-2, 0, 2, 4, 6, 8]
	append(---)
	inject_at(---)
	fmt.println(arr2)
}
